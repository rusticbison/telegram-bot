#!/usr/bin/env python

"""faqbot.py: Replies with most recent update."""

__author__      = "Justin Smith"
__copyright__   = "Copyright 2017"

import requests
import time
import redis

saved_message_id = 0
update_id = '753368821'

#keywords to listen for
keyword_in_chat = 'best wallet'
keyword_in_chat2 = 'monero wallet'
keyword_in_chat3 = 'freewallet'
keyword_in_chat4 = 'which exchanges'

#replies
bot_comment = 'The best Monero wallet is the reference client. Download it here:\n https://getmonero.org/downloads/ \nThis is also a light wallet. Follow this guide and you\'ll be ready to use Monero in no time at all:\n https://getmonero.org/resources/user-guides/remote_node_gui.html'
bot_comment2 = 'Freewallet is a well known scam. For a full list of known scams, see: https://www.reddit.com/r/Monero/wiki/avoid'
bot_comment3 = 'Monero is listed on many exchanges. See the full list here: https://www.coingecko.com/en/coins/monero/trading_exchanges'

while True:

    #global
    bot_token = 'bot505323297:AAEyv3_0A90muPmRbq5cNqRz3ATZwCc6awQ'

    #chat ids, we'll choose one
    private_chat = ''
    group_chat_cryptoChat = ''
    group_chat_Monero_Enthusiasts = '-1001052238089'

    #choose the chat id
    chat_where_reply_is_sent = group_chat_Monero_Enthusiasts



    #monitor the chat
    task = 'getUpdates?offset={}'.format(update_id)
    url = 'https://api.telegram.org/{}/{}'.format(bot_token, task)
    print(url)
    response = requests.get(url)
    json = response.json()

    try:
        update_id = json['result'][-1]['update_id']
        haystack = json['result'][-1]['message']['text']

        #debug:
        #result_chat_id = json['result'][-1]['message']['chat']['id']
    except: haystack = ''

    print(haystack)
    print(update_id)

    #check to see if message id has changed?
    try:
        current_message_id = json['result'][-1]['message']['message_id']
        # last_processed_id = json['result'][-1]['update_id']

    #debug
        # print(current_message_id)
        # print(last_processed_id)
    except: current_message_id = 'failure to get message id'

    #check to see if the current message id is the same as what is in the database
    if current_message_id != saved_message_id:

        if (keyword_in_chat in haystack) | (keyword_in_chat2 in haystack):
            params = {'chat_id': chat_where_reply_is_sent, 'text': bot_comment}
            task = 'sendMessage'
            url = 'https://api.telegram.org/{}/{}'.format(bot_token, task)
            response = requests.post(url, data=params)
            print('sent message')

            # update the message in the database
            saved_message_id = current_message_id
            saved_update_id = update_id


        if keyword_in_chat3 in haystack:
            params = {'chat_id': chat_where_reply_is_sent, 'text': bot_comment2}
            task = 'sendMessage'
            url = 'https://api.telegram.org/{}/{}'.format(bot_token, task)
            response = requests.post(url, data=params)
            print('sent message')

            # update the message in the database
            saved_message_id = current_message_id
            update_id = update_id +1
            update_id = json['result'][-1]['update_id']
            saved_update_id = update_id


        if keyword_in_chat4 in haystack:
            params = {'chat_id': chat_where_reply_is_sent, 'text': bot_comment3}
            task = 'sendMessage'
            url = 'https://api.telegram.org/{}/{}'.format(bot_token, task)
            response = requests.post(url, data=params)
            print('sent message')

            # update the message in the database
            saved_message_id = current_message_id
            update_id = update_id
            update_id = json['result'][-1]['update_id']
            saved_update_id = update_id


    #wait a few seconds before checking the chat group again for new messages
    time.sleep(2)
